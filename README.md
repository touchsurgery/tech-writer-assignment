# Tech Writer Assignment
---

Digital Surgery, a company which has users mostly in USA, is ready to release its latest app update to customers. Using the five tickets below, provide us, in a PDF file with a brief 'release notes' we could send to our customers in two forms:

1. Via Email
1. App Store / Play Store release notes 

## Tickets relevant to the release

### 1 - [Bug] Password field on the login page is using the email keyboard

> The field type for the password is using the email keyboard. Hardly noticable issue for the end user. Just a different keyboard was being presented.

### 2 - [Bug] [Not Fixed] Samsung S5 and iPhone 5S won't display splash screen

> Whenever an using with these phones open the app, the logo of Digital Surgery is not presented.

### 3 - [Bug] Last modified information incorrectly preserved on Library Update

> Given I open at a procedure  
> And perform some steps  
> And I restart the app  
> When I open the same procedure  
> Then details and icons are missing  

### 4 - [Feature] Account Settings - Marketing opt in/out

> As a user, when I navigate to the account settings, I would like to see a 'Marketing opt in/out' option.  
> As a user, when I click on the 'Marketing opt in/out' checkbox, I would like my marketing preference to be updated accordingly.

### 5 - [Feature] Implement push notifications using AWS Pinpoint SDK

> At the moment we're using Braze & Appboy to send push-notifications to our app's customers. The implementation of this ticket should cover deprecating the old libraries and use the new AWS Pinpoint SDK for that. User shouldn't notice any change.